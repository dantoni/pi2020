<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Todo;
use Faker\Generator as Faker;
use League\CommonMark\Block\Element\Paragraph;

$factory->define(todo::class, function (Faker $faker) {
    return [
        'name'=> $faker->sentence(3),
        'description'=> $faker->paragraph(4),
        'completed' => false,
        //
    ];
});
